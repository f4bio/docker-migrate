FROM golang:latest
MAINTAINER Fabio Tea

COPY . /build
RUN export GO111MODULE=on
RUN go test ./...
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o /app/docker-migrate .

CMD ["/app/docker-migrate"]
