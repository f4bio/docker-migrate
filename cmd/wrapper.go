package cmd

import (
  "f4b.io/docker-migrate/globals"
)

var Log = globals.Log

func CheckErrorAndExit(err error) {
  globals.CheckErrorAndExit(err)
}
