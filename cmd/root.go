package cmd

import (
  "f4b.io/docker-migrate/globals"
  "github.com/manifoldco/promptui"
  "github.com/spf13/cobra"
  "github.com/spf13/viper"
  "os"
)

var rootCmd = &cobra.Command{
  Use:   "docker-migrate",
  Short: "docker-migrate docker-migrate",
  Long:  `A real discord voice reaction bot`,
  Run: func(cmd *cobra.Command, args []string) {
    var err error

    prompt := promptui.Select{
      Label: "Select Day",
      Items: []string{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
        "Saturday", "Sunday"},
    }

    _, result, err := prompt.Run()

    if err != nil {
      Log.Debugf("Prompt failed %v\n", err)
      return
    }

    Log.Debugf("You choose %q\n", result)

  },
}

func init() {
  Log.Debug("init")

  cobra.OnInitialize(initConfig)

  // Here you will define your flags and configuration settings.
  // Cobra supports persistent flags, which, if defined here,
  // will be global for your application.
  rootCmd.PersistentFlags().StringVar(
    &globals.DiscordToken, "discordToken", viper.GetString("token"), "discordToken")
  rootCmd.PersistentFlags().StringVar(
    &globals.BotName, "name", viper.GetString("name"), "bot name")
  rootCmd.PersistentFlags().StringVar(
    &globals.GoogleKey, "googleKey", viper.GetString("googleKey"), "googleKey")
  rootCmd.PersistentFlags().StringVar(
    &globals.AdminName, "adminName", viper.GetString("adminName"), "adminName")
  rootCmd.PersistentFlags().StringVar(
    &globals.HookerName, "hookerName", viper.GetString("hookerName"), "hookerName")
  rootCmd.PersistentFlags().StringVar(
    &globals.ChannelName, "channelName", viper.GetString("channelName"), "channelName")
  rootCmd.PersistentFlags().StringVarP(
    &globals.ConfigFile, "config", "c", viper.GetString("config"), "config file")
  rootCmd.PersistentFlags().StringVarP(
    &globals.SoundsDirectory, "sounds", "s", "", "sounds directory")
  rootCmd.PersistentFlags().StringVar(
    &globals.AwsAccessKey, "awsAccessKey", viper.GetString("awsAccessKey"), "aws access key")
  rootCmd.PersistentFlags().StringVar(
    &globals.AwsSecretKey, "awsSecretKey", viper.GetString("awsSecretKey"), "aws secret key")
  rootCmd.PersistentFlags().StringVar(
    &globals.PastebinAPIKey, "pastebinApiKey", viper.GetString("pastebinApiKey"), "pastebin api key")

  // Cobra also supports local flags, which will only run
  // when this action is called directly.
}

func initConfig() {
  viper.SetDefault("name", globals.DefaultBotName)
  viper.SetDefault("config", globals.DefaultConfigFile)
  viper.SetDefault("sounds", globals.DefaultSoundsFolder)
  viper.SetDefault("channelName", globals.DefaultChannelName)

  // Don't forget to read config either from cfgFile or from home directory!
  if globals.ConfigFile != "" {
    viper.SetConfigFile(globals.ConfigFile)
  } else {
    viper.SetConfigFile(globals.DefaultConfigFile)
  }

  viper.AutomaticEnv() // read in environment variables that match

  err := viper.ReadInConfig()
  // If a config file is found, read it in.
  if err == nil {
    Log.Info("Using config file:", viper.ConfigFileUsed())
  } else {
    Log.Error("Error using config file:", err)
  }

  for idx, key := range viper.AllKeys() {
    Log.Debugf("config > idx=%d key=%s value=%s", idx, key, viper.Get(key))
  }

  globals.DiscordToken = viper.GetString("discordToken")
  globals.SoundsDirectory = viper.GetString("sounds")
  globals.BotName = viper.GetString("name")
  globals.AwsAccessKey = viper.GetString("awsAccessKey")
  globals.AwsSecretKey = viper.GetString("awsSecretKey")
  globals.HookerName = viper.GetString("hookerName")
  globals.AdminName = viper.GetString("adminName")
  globals.ChannelName = viper.GetString("channelName")
}

// Execute starting point of everything
func Execute() {
  // CPU profiling by default
  //defer profile.Start(profile.CPUProfile).Stop()
  // Memory profiling
  //defer profile.Start(profile.MemProfile).Stop()

  var err error
  err = viper.BindPFlag("discordToken", rootCmd.PersistentFlags().Lookup("discordToken"))
  err = viper.BindPFlag("name", rootCmd.PersistentFlags().Lookup("name"))
  err = viper.BindPFlag("googleKey", rootCmd.PersistentFlags().Lookup("googleKey"))
  err = viper.BindPFlag("adminName", rootCmd.PersistentFlags().Lookup("adminName"))
  err = viper.BindPFlag("hookerName", rootCmd.PersistentFlags().Lookup("hookerName"))
  err = viper.BindPFlag("adminName", rootCmd.PersistentFlags().Lookup("adminName"))
  err = viper.BindPFlag("config", rootCmd.PersistentFlags().Lookup("config"))
  err = viper.BindPFlag("sounds", rootCmd.PersistentFlags().Lookup("sounds"))
  err = viper.BindPFlag("awsAccessKey", rootCmd.PersistentFlags().Lookup("awsAccessKey"))
  err = viper.BindPFlag("awsSecretKey", rootCmd.PersistentFlags().Lookup("awsSecretKey"))
  err = viper.BindPFlag("pastebinApiKey", rootCmd.PersistentFlags().Lookup("pastebinApiKey"))
  if err != nil {
    os.Exit(1)
  }

  if err := rootCmd.Execute(); err != nil {
    os.Exit(1)
  }
}
