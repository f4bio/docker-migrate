package main

import (
  "f4b.io/docker-migrate/cmd"
)

func init() {
  InitApp()
}

// InitApp initializes the core
func InitApp() {
  Log.Info("f4b.io/docker-migrate: initialized!")
}

func main() {
  cmd.Execute()
}
